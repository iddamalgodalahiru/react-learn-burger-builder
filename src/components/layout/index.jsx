import React from "react";
import Auc from '../../hoc/aux';

import './styles.scss';
const layout = (props) => (
    <Auc>
          <div>
        Navigation Bar
    </div>

    <main className="layout-container">
        {props.children}
    </main>
    </Auc>
  
)

export default layout;