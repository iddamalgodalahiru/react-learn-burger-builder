import React from "react";

import BuildControl from './buildControl/index'

import "./styles.scss";

const controls = [
    {
        label: 'Salad', type: 'salad'
    },
    {
        label: 'Bacon', type: 'bacon'
    },
    {
        label: 'Cheese', type: 'cheese'
    },
    {
        label: 'Meat', type: 'meta'
    },
];

const BuildControls = () => {

    return(
          <div className="build-Controls">
              {controls.map(ctrl =>(
                 <BuildControl key={ctrl.label} label={ctrl.label}/>

              ))}
          </div>
    )
}

export default BuildControls;