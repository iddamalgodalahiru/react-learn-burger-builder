import React from "react";

import "./styles.scss";

const BuildControl = (props) => {

    return(
   <div className="build-Control">
                <div className="label">{props.label}</div>
                <button className="less">Less</button>
                <button className="more">More</button>
            </div>
    )
}

export default BuildControl;