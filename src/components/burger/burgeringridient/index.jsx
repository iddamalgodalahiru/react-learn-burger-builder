import React from "react";
import PropTypes from "prop-types";

import Classes from "./styles.scss";

const BurgerIngredients = (props) => {

    let ingredients = null;


    switch(props.type) {

        case('bread-bottom'):
            ingredients = <div className='breadBottom'></div>;
            break;
        case('bread-top'):
            ingredients = (
                <div className="breadTop">
                    <div className="seeds1"></div>
                    <div className="seeds2"></div>

                </div>
            );
            break;
        case('meat'):
            ingredients = <div className="meat"></div>;
            break;
        case('cheese'):
            ingredients = <div className="cheese"></div>;
            break; 
        case('salad'):
            ingredients = <div className="salad"></div>;
            break;
        case('bacon'):
            ingredients = <div className="bacon"></div>;
            break;
        default:
            ingredients=null;

    }

    return ingredients;

}
BurgerIngredients.propTypes = {
    type: PropTypes.string.isRequired

}

export default BurgerIngredients;