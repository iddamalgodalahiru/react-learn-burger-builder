import React from "react";

import Ingredients from "./burgeringridient";
import classes from './styles.scss';

const Burger = (props) =>{

    // const { ingredients} = props;

    // convert object to array
    let transformIngredients = Object.keys(props.ingredients)
          .map(igKey => {
                      //create new array for each ingredient by igKey
              return [...Array(props.ingredients[igKey])].map(( _,i) =>{

                 return <Ingredients key={igKey + i} type={igKey}/>;
              })
          })
          .reduce((arr ,el) =>{
                      console.log('arr',arr);
                      console.log('el',el)
                      return arr.concat(el)

          }, []);

        if(transformIngredients.length === 0) {
            transformIngredients = <p>Please start adding ingredient</p>
        }          
          
return(
    <div className="burger">
        <Ingredients type="bread-top"/>
         {transformIngredients}
        <Ingredients type="bread-bottom"/>

     </div>
)
}

export default Burger;