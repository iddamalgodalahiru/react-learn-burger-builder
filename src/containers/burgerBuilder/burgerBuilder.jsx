import React, { Component } from 'react'

import Aux  from "../../hoc/aux";
import Burger from '../../components/burger/index';
import BuildControls from '../../components/burger/buidControls/index';

class BurgerBuilder extends Component {

  state = {
    ingredients: {
      salad: 0,
      bacon: 0,
      cheese:0,
      meat: 0
    }
  }

  render() {
    return (
     <Aux>
         <div>
             <div><Burger ingredients={this.state.ingredients}/></div>
             <div><BuildControls/></div>
         </div>
     </Aux>
    )
  }
}

export default BurgerBuilder;
