import React from 'react';
import Layout from './components/layout';
import BurgerBuilder from "./containers/burgerBuilder/burgerBuilder";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Layout>
          <BurgerBuilder/>
        </Layout>
      </header>
    </div>
  );
}

export default App;
